#!/usr/bin/perl
use CGI;                         
$result = qx("./recursos");	
$result =~ s/$/<br>/mg; #reemplaza salto de linea por <br>

$q = CGI->new;                      
print $q->header,                   
      #$q->start_html('Recursos servidor'),
	  '<form action="../index.html">
      <input type="submit" value="Volver al indice" />
      </form>',
      $q->head('<meta http-equiv="refresh" content="5">');

output_top($q);

#print $q->h1('Recursos');         
	  print "$result";
      $q->end_html;

output_end($q);




    sub output_top {
        my ($q) = @_;
        print $q->start_html(
            -title => 'Monitor de recursos',
            -bgcolor => 'white',
            -style => {
	        -code => '
                    /* Stylesheet code */
                    body {
                        font-family: verdana, sans-serif;
                    }
                    h2 {
                        color: darkblue;
                        border-bottom: 1pt solid;
                        width: 100%;
                    }
                    div {
                        text-align: right;
                        color: steelblue;
                        border-top: darkblue 1pt solid;
                        margin-top: 4pt;
                    }
                    th {
                        text-align: right;
                        padding: 2pt;
                        vertical-align: top;
                    }
                    td {
                        padding: 2pt;
                        vertical-align: top;
                    }
                    /* End Stylesheet code */
                ',
	    },
        );
        print $q->h2("Monitor de recursos");
    }

    # Outputs a footer line and end html tags
    sub output_end {
        my ($q) = @_;
        print $q->div("SO2-TP3 by Matias Lionel Ceballos");
        print $q->end_html;
    }
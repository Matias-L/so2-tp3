#include <stdio.h>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <string.h>
#include <time.h>

/* Autor: Ceballos, Matias Lionel.
Punto (a):
Página que reporte información sobre recursos
varios del sistema embebido:
• Procesador, consumo
• Memoria
• Uptime
• Fecha y Hora actual




*/




int main(int argc, char const *argv[])
{

	

 	time_t rawtime;
  	struct tm * timeinfo;
	time ( &rawtime );
  	timeinfo = localtime ( &rawtime );

	struct sysinfo estructuraInformacion;
	sysinfo(&estructuraInformacion);	//Obtengo datos del sistema
	long time=estructuraInformacion.uptime;
	long mem=estructuraInformacion.freeram;
	float cpu=estructuraInformacion.loads[1]/(float)(1 << SI_LOAD_SHIFT);
	

	//long minuto = 60;			//Variables usadas para acomodar el formato de 
 	//long hora = minuto * 60;	//salida del uptime
 	long segundos = time;
 	long dias = segundos / (24*3600);
 	segundos = segundos % (24*3600);
 	long horas = segundos / 3600;
 	segundos = segundos % 3600;
 	long minutos = segundos / 60;
 	segundos = segundos % 60;
 	//long segundos = time;	



/*
	FILE* archivo;
	archivo=fopen("recursos.html","w");


	fprintf(archivo, "<!DOCTYPE html>\n");
	fprintf(archivo, "<html>\n");
	fprintf(archivo, "<body>\n");

	fprintf(archivo, "<h1>super mostrador de recursos </h1>\n");
	fprintf(archivo, "<p>Uptime: %li [seg]\n</p>", time);
	fprintf(archivo, "<p>Uptime: %li [d] : %li [h]: %li [min]: %li [seg]\n</p>", dias, horas, minutos, segundos);
	fprintf(archivo, "<p>Memoria libre: %li [B]</p>\n", mem);
	fprintf(archivo, "<p>Uso Procesador: %f \n</p>", cpu);
	fprintf(archivo, "<p>Fecha y hora local: %s </p>", asctime (timeinfo) );

	fprintf(archivo, "</body>\n");
	fprintf(archivo, "</html>\n");	



	fclose(archivo);

*/


//printf("Ahora: %li [seg]\n", asctime(timeinfo));
printf("Uptime: %li [d] : %li [h]: %li [min]: %li [seg]\n", dias, horas, minutos, segundos);
printf("Memoria libre: %li [B]\n", mem);
printf("Uso Procesador: %f \n", cpu);
printf("Fecha y hora local: %s", asctime (timeinfo) );

	return 0;
}

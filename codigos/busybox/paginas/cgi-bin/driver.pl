#!/usr/bin/perl
use CGI;
use strict;
use File::Basename;
use Data::Dumper;

my $result = qx("lsmod"); #Guarda el listado de modulos instalados
$result =~ s/$/<br>/mg; #reemplaza salto de linea por <br>
#$result =~ s/\s/</td><td>/g;
#$result = "<td>$result</td>";

my $PROGNAME = "driver.pl"; #nombre del programa. Usado en el HTML
my $cgi = new CGI();	

#Muestra el boton que me permite volver al indice
print "Content-type: text/html\n\n";
print
'<form action="../index.html">
    <input type="submit" value="Volver al indice" />
</form>';
output_top($cgi);

#Pregunto si se entra por primera vez, o mediante la accion del formulario
if (! $cgi->param("button") ) {
	DisplayForm();	#Invoca al sub DisplayForm
	output_end($cgi);
	exit;
} else{ #Invocado desde el formulario
	#Obtengo las variables ingresadas por el formulario
	my $upfile = $cgi->param('upfile');	
	my $modeliminar = $cgi->param('modeliminar');
	if(length($upfile)){#Reviso si se pide subir un driver (campo driver no nulo)
		my $basename = GetBasename($upfile); #Obtiene el nombre de archivo
		my ($nom,$path,$ext) = fileparse("$basename", qr"\..[^.]*$");#Obtiene extencion
		print "Extencion del archivo: $ext<br>";
		if("$ext" eq ".ko"){
			print "Extencion valida. Instalando<br>";
			no strict 'refs'; #Ver si hace falta. Supuestamente en versiones nuevas de perl no
		        #apaga chequeo de referencia, para poder usar el nombre de archivo como un 
			#filehandle
			if (! open(OUTFILE, ">./$basename") ) { #Guardo lo recibido en un archivo
				print "Can't open /www/localhost/htdocs for writing - $!";
				exit(-1);
			}
			print "Guardando archivo en: /www/localhost/htdocs/$basename<br>\n";#Muestra un mensaje mientras se guarda
			#Comienzo a guardar en el archivo
			my $nBytes = 0;
			my $totBytes = 0;
			my $buffer = "";
			binmode($upfile); #Supuestamente no hace falta. Comentar y probar
			while ( $nBytes = read($upfile, $buffer, 1024) ) {
				print OUTFILE $buffer;
				$totBytes += $nBytes;
			}
			close(OUTFILE);
			use strict 'refs';#Enciende nuevamente chequeo de referencias
			print "Archivo $basename cargado con exito ($totBytes bytes)<br>\n";#Mensaje una vez cargado	
			#Intenta instalar el modulo
			my $instalacion=`insmod ./$basename`;
			print "$instalacion";
		}#Fin if extencion valida
		else{
			print "Extencion NO valida. Alto<br>";
		}
	}#Fin if modulo a subir
	if(length($modeliminar)){

		print "Queres eliminar $modeliminar";
		my $desinstalacion=`rmmod $modeliminar`;
		print "$desinstalacion";
	}
	}#Fin else "invocado desde el formulario"

			output_end($cgi);

##############################################
# Subrutinas
##############################################
#
# GetBasename - Devuelve el nombre de archivo desde el path completo
#
sub GetBasename {
	my $fullname = shift;
	my(@parts);
	# check which way our slashes go.
	if ( $fullname =~ /(\\)/ ) {
		@parts = split(/\\/, $fullname);
	} else {
		@parts = split(/\//, $fullname);
	}
	return(pop(@parts));
}
#
# DisplayForm - muestra el formulario HTML
#
sub DisplayForm {
print <<"HTML";
<html>
<head>
<!--
<title>Administracion de drivers</title>
-->
<body>
<!--
<h1>Administracion de drivers</h1>
-->
<form method="post" action="$PROGNAME" enctype="multipart/form-data">
<center>
Ingrese el archivo a subir: <input type="file" accept=".ko" name="upfile"><br>
O escriba el modulo a eliminar: <input type="text" name="modeliminar"><br>
<input type="submit" name="button" value="Ejecutar">
</center>
</form>

<h2 style="color:darkblue"> Modulos instalados </h2>
<p>$result</p>;
HTML
}




    sub output_top {
        my ($q) = @_;
        print $q->start_html(
            -title => 'Administracion de drivers',
            -bgcolor => 'white',
            -style => {
	        -code => '
                    /* Stylesheet code */
                    body {
                        font-family: verdana, sans-serif;
                    }
                    h2 {
                        color: darkblue;
                        border-bottom: 1pt solid;
                        width: 100%;
                    }
                    div {
                        text-align: right;
                        color: steelblue;
                        border-top: darkblue 1pt solid;
                        margin-top: 4pt;
                    }
                    th {
                        text-align: right;
                        padding: 2pt;
                        vertical-align: top;
                    }
                    td {
                        padding: 2pt;
                        vertical-align: top;
                    }
                    /* End Stylesheet code */
                ',
	    },
        );
        print $q->h2("Administracion de drivers");
    }

    # Outputs a footer line and end html tags
    sub output_end {
        my ($q) = @_;
        print $q->div("SO2-TP3 by Matias Lionel Ceballos");
        print $q->end_html;
    }
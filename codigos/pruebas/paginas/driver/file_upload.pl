#!/usr/bin/perl
use CGI;
use strict;

my $result = qx("lsmod");
$result =~ s/$/<br>/mg; #reemplaza salto de linea por <br>

#my @salto="\n";
#my @parrafo="</p><p>"
#$result =~ s/$/<br>/mg; #reemplaza salto de linea por <br>
#$result =~ s/\s{2}/_/mg; #cada 2 espacios los reemplaza por un guion
#$result =~ s/\s{2}/_/mg; #cada 2 espacios los reemplaza por un guion

#my @result = qx("lsmod");
#my @result = qx("./listaGOES");
my $PROGNAME = "file_upload.pl";

my $cgi = new CGI();	
print "Content-type: text/html\n\n";
print
'<form action="../index.html">
    <input type="submit" value="Volver al indice" />
</form>';

#	  print "$result";
	  #print "hola \n salto \n linea\n";
#
# If we're invoked directly, display the form and get out.
#
if (! $cgi->param("button") ) {
	DisplayForm();	#Invoca al sub DisplayForm
	exit;
}

#
# We're invoked from the form. Get the filename/handle.
#
my $upfile = $cgi->param('upfile');

#
# Get the basename in case we want to use it.
#
my $basename = GetBasename($upfile);

#
# At this point, do whatever we want with the file.
#
# We are going to use the scalar $upfile as a filehandle,
# but perl will complain so we turn off ref checking.
# The newer CGI::upload() function obviates the need for
# this. In new versions do $fh = $cgi->upload('upfile'); 
# to get a legitimate, clean filehandle.
#
no strict 'refs';
#my $fh = $cgi->upload('upfile'); 
#if (! $fh ) {
#	print "Can't get file handle to uploaded file.";
#	exit(-1);
#}

#######################################################
# Choose one of the techniques below to read the file.
# What you do with the contents is, of course, applica-
# tion specific. In these examples, we just write it to
# a temporary file. 
#
# With text files coming from a Windows client, probably
# you will want to strip out the extra linefeeds.
########################################################

#
# Get a handle to some file to store the contents
#
if (! open(OUTFILE, ">./$basename") ) {
	print "Can't open /www/localhost/htdocs for writing - $!";
	exit(-1);
}

# give some feedback to browser
print "Saving the file to /www/localhost/htdocs<br>\n";

#
# 1. If we know it's a text file, strip carriage returns
#    and write it out.
#
#while (<$upfile>) {
# or 
#while (<$fh>) {
#	s/\r//;
#	print OUTFILE "$_";
#}

#
# 2. If it's binary or we're not sure...
#
my $nBytes = 0;
my $totBytes = 0;
my $buffer = "";
# If you're on Windows, you'll need this. Otherwise, it
# has no effect.
binmode($upfile);
#binmode($fh);
while ( $nBytes = read($upfile, $buffer, 1024) ) {
#while ( $nBytes = read($fh, $buffer, 1024) ) {
	print OUTFILE $buffer;
	$totBytes += $nBytes;
}

close(OUTFILE);

#
# Turn ref checking back on.
#
use strict 'refs';

# more lame feedback
print "Archivo $basename cargado con exito ($totBytes bytes)<br>\n";	
#Intenta instalar el modulo
my $instalacion=`./module $basename 0`;
print "$instalacion";

##############################################
# Subroutines
##############################################

#
# GetBasename - delivers filename portion of a fullpath.
#
sub GetBasename {
	my $fullname = shift;

	my(@parts);
	# check which way our slashes go.
	if ( $fullname =~ /(\\)/ ) {
		@parts = split(/\\/, $fullname);
	} else {
		@parts = split(/\//, $fullname);
	}

	return(pop(@parts));
}

#
# DisplayForm - spits out HTML to display our upload form.
#
sub DisplayForm {
print <<"HTML";
<html>
<head>
<title>Upload Form</title>
<body>
<h1>Upload Form</h1>
<form method="post" action="$PROGNAME" enctype="multipart/form-data">
<center>
Enter a file to upload: <input type="file" name="upfile"><br>
<input type="submit" name="button" value="Upload File">
</center>
</form>

<h1> Modulos instalados </h1>
<p>$result</p>;
HTML
}
#<h1> Modulos instalados </h1>
#<p>$result</p>;
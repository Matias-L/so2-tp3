#include "cabezeras.h"
/**
	@file funciones.c
	@author Ceballos, Matias Lionel
	@date Abril 2019

*/
/**
	@brief Funcion que se ejecuta luego de haber ingresado usuario y contraseña válidos.

	Consta de un bucle que se ejecuta durante todo el ciclo de vida del cliente.

	@param int Socket file descriptor
*/

void sesionActiva(int socket){

	char buffer[TAM];
	int n=0;
	int sesionActiva=1;		//Posiblemente no sea necesario usarlo. Revisar
	printf(COLOR_VERDE); 
	printf("FIRMWARE: %s\n", VERSION);
	printf(COLOR_RESET); 

	while(sesionActiva){
	printf(COLOR_AMARILLO);
    printf("Esperando instrucciones...\n");
	printf(COLOR_RESET);

	memset( buffer, '\0', TAM );
	n = read( socket, buffer, TAM );	//Leo el comando enviado por el servidor
	   	if ( n < 0 ) {					//y lo voy comparando
		perror( "lectura de socket" );
	    exit( 1 );
	    }		
		if(!strcmp(buffer, "updateFirmware")){ 
		n=updateFirmware(socket);	
		sesionActiva=0;
		printf("SALIENDO\n");	  
		}
		if(!strcmp(buffer, "startScanning")){
		n=startScanning(socket);
		//sesionActiva=0;
		//printf("SALIENDO\n");
		}
		if(!strcmp(buffer, "obtenerTelemetria")){
		n=obtenerTelemetria(socket);
		}
		if(!strcmp(buffer, "clienteExit")){
		printf(COLOR_ROJO);
		printf("Cerrando comunicacion. Apagando...\n");
		printf(COLOR_RESET);
		close(socket);
		exit(0);
		}
	}//Fin while sesion activa
}
/**
	@brief Actualiza ejecutable con binario provisto por el servidor.

	Recibe primero el tamaño del binario para saber cuanto leer del socket.
	Mientras se va recibiendo, se guarda en un archivo nuevo.
	Al finalizar la recepcion, se invoca "exec" para reemplazar el 
	proceso actual por el nuevo ejecutable	

	@param Socket desde donde se escuchará la cantidad a recibir
	@return nada...creo tendria que ser void
*/

int updateFirmware(int socket){
	printf("Preparando actualizacion\n");	

	int size=0;
	char nuevoNombre[TAM];
	int bytesLeidos=0;
	FILE* archivo;
	char buffer[TAM];
	char comando[TAM];

	memset(nuevoNombre, '\0', TAM);
	memset(buffer, '\0', TAM);

	int n = read( socket, buffer, TAM );	//Recibo el nuevo nombre desde el servidor	
	   	if ( n < 0 ) {					
	    perror( "lectura de socket" );
	    exit( 1 );
		}
	strcpy(nuevoNombre, buffer);

	n = read( socket, &size, sizeof(int) );	//Recibo el tamaño del binario
    	if ( n < 0 ) {
        perror( "lectura de socket" );
        exit( 1 );
        }
	printf("Tamanio a recibir: %d \n", size);

	archivo=fopen(nuevoNombre, "w");		//Genero un archivo con el nombre recibido
	//rewind(archivo);						//vuelvo el puntero al inicio (¿es necesario? probar sin)

		while(bytesLeidos<size){			//Voy escribiendo el binario mientras falten datos
		memset( buffer, '\0', TAM);			//del servidor
		n = read( socket, buffer, TAM );
			if ( n < 0 ) {
			perror( "escritura en socket" );
			exit( 1 );
			}
		bytesLeidos = bytesLeidos + n;
		printf("%d\n",bytesLeidos );
		fwrite(buffer, 1, sizeof(buffer), archivo);
		}

	printf("Levantando nueva version...\n");
	//Formo el comando que quiero ejecutar en execvp para levantar la nueva version
	memset(comando, '\0', TAM);
	strcpy(comando, "./");
	strcat(comando, nuevoNombre);

	printf("Nombre del nuevo binario: %s\n", nuevoNombre);
	fclose(archivo);

	//La sintaxis de la familia exec me exige enviar el nombre
	//y parametros del programa a ejecutar
	//como una arreglo del mismo estilo que main.
	//Entonces, armo el arreglo

	close(socket);		//Cierro el socket con el servidor para reiniciar la conexion
	system("./resetScript.sh &");
	printf("MATANDO\n");
	exit(0);
	/*
	char *args[2]={comando, NULL};
	printf("Ejecutando sobre: %s\n",comando );
	execvp(args[0], args);
	*/
	//En principio nunca deberia llegar aca
	//¿cambiar la funcion a void?
	return 0;
}
/**
	@brief Envia los fragmentos de imagen almacenados por el cliente

	El archivo de imagen debe haber sido fragmentado previamente. El tamaño
	del fragmento esta definido por BYTES_STREAM en la cabezera.
	Se envia cada fragmento completo como un mensaje individual.

	@param socket file descriptor sobre el cual se realiza la comunicacion
	@return ¿cambiar a void?
*/

int startScanning(int socket){
	printf(COLOR_MAGENTA);
	printf("Escaneando...\n");
	printf(COLOR_RESET);

	int n=0;
	int cantidadFragmentos=NRO_FRAGMENTOS-1; //CAMBIAR CUANDO CAMBIE LA IMAGEN
	int fragmentoNumero=1;
	FILE *fragmento;
	char nombreFragmento[TAM];
	char directorio[TAM];
	char buffer[BYTES_STREAM];
	memset( buffer, '\0', BYTES_STREAM );
	sprintf(buffer,"%d", cantidadFragmentos);
	n=write( socket, buffer, sizeof(buffer) );
	   	if ( n < 0 ) {
	    perror( "escritura de socket" );
	    close(socket);
	    exit( 1 );
	    }
	//Almaceno en la variable directorio la ubicacion
	//de la carpeta con los fragmentos de imagen
	//memset( directorio, '\0', TAM );
	//strcpy(directorio, DIRECTORIO_IMAGEN);

	//Convierto al entero "fragmento numero" a string 
	//para luego abrir el archivo
	
		while(fragmentoNumero<=cantidadFragmentos){
		//Limpio la variable directorio para que no se vayan
		//acumlando las concatenaciones
		memset( directorio, '\0', TAM );
		strcpy(directorio, DIRECTORIO_IMAGEN);
		//Limpio el nombre del fragmento entre llamada y llamada
		memset( nombreFragmento, '\0', TAM );
		//Convierto el entero que representa el numero de
		//fragmento a string para poder operar
		sprintf(nombreFragmento, "%d", fragmentoNumero);
		//Concateno con el directorio para poder apuntar
		//a la imagen que se va a abrir
		strcat(directorio,nombreFragmento );
	 	fragmento = fopen(directorio,"r");
	 		
		//Descomentar en version 1, comentar en la 2.
		printf("Enviando %s de %d\n", directorio ,cantidadFragmentos);

		fread(buffer,1,sizeof(buffer),fragmento);
		n=write( socket, buffer, sizeof(buffer) );
		   	if ( n < 0 ) {
		    perror( "escritura de socket" );
		    close(socket);
		    exit( 1 );
		    }
		fclose(fragmento);
	    fragmentoNumero++;
		}//Fin while
	return 0;
}

/**
	@brief obtenerTelemetria. Envia datos del estado del cliente

	Envia uptime, version del binario, RAM libre y promedio de carga de procesador en el ultimo minuto.
	Levanta un socket no orientado a la conexion como servidor para enviar los datos de telemetria
	y cierra dicho socket antes de retornar

	@param socket file descriptor orientado a la conexion desde donde se estaba realizando 
	la comunicacion
	@return (¿deberia ser void)
*/

int obtenerTelemetria(int socketfd){
	printf(COLOR_MAGENTA);
	printf("Preparando Telemetria...\n");
	printf(COLOR_RESET);
	long minuto = 60;			//Variables usadas para acomodar el formato de 
 	long hora = minuto * 60;	//salida del uptime
 	long dia = hora * 24;		//probar con "define" desde las cabezeras
 								//para no desperdiciar memoria.

	char buffer[TAM2];
	struct sysinfo estructuraInformacion;
	memset( buffer, '\0', TAM2 );
	sysinfo(&estructuraInformacion);	//Obtengo datos del sistema
	int descriptor_socket, resultado;
	struct sockaddr_un struct_cliente;
	/* Creacion de socket */
		if(( descriptor_socket = socket(AF_UNIX, SOCK_DGRAM, 0) ) < 0 ){ 
		perror("socket" );
		}
	/* Inicialización y establecimiento de la estructura del cliente */
	memset( &struct_cliente, 0, sizeof( struct_cliente ) );
	struct_cliente.sun_family = AF_UNIX;
	strncpy( struct_cliente.sun_path, DIR_SOCK_TELE, sizeof( struct_cliente.sun_path ) );

	long tiempo=estructuraInformacion.uptime;
	long mem=estructuraInformacion.freeram;
	float cpu=estructuraInformacion.loads[1]/(float)(1 << SI_LOAD_SHIFT);
	//Como son 4 datos a mostrar, cada vez que envio un dato aumento el indice
	//para ir enviando otro dato
		for(int i=0; i<4;i++){		
		memset( buffer, '\0', TAM2 );
			switch(i){
			case 0:
			sprintf(buffer,"ID satelite: %d",getpid());	//Tomo como id del satelite al pid del proceso actual
			break;	
			case 1:
			sprintf (buffer, "Uptime : %ld dias, %ld:%02ld:%02ld", 
					tiempo / dia, (tiempo % dia) / hora,
					(tiempo % hora) / minuto, tiempo % minuto);
			break;
			case 2:
				sprintf(buffer,"Version: %s", VERSION);
			break;
			case 3:
				sprintf(buffer,"Carga CPU: %f - Memoria libre: %ld", cpu, mem);
			break;
			default:
			break;
			}
		sleep(1);
		/* Envío de datagrama al servidor */
		resultado = sendto( descriptor_socket, buffer, TAM2, 0, (struct sockaddr *)&struct_cliente, sizeof(struct_cliente) );
			if( resultado < 0 ) {
			perror( "sendto" );
			exit( 1 );
			}
		printf(COLOR_VERDE);
		printf("Enviado:- %s\n", buffer);
		printf(COLOR_RESET);
		}//fin for
	//finaliza socket sin conexion
	close(descriptor_socket);
	return 0;
}

/**	@file main.c
	@brief Funcion principal del cliente ( satelite).

	Comienza creando un socket Unix orientado a la conexión. El usuario debe colocar por linea de 
	comandos el socket a utilizar (primero debe haber sido creado por el 
	servidor. Por defecto, está en la carpeta del servidor). 
	Una vez conectado al servidor, se ingresa a la función login:
	@code 
	int login(int socket_filedescriptor)

	La misma recibe el prompt de usuario invitado enviado por el servidor, y maneja el proceso
	de validación de credenciales.
	Si se realiza la validación, se ingresa a la función correspondiente a una sesión activa:
	@code
	int sesionActiva(int socket_filedescriptor)
   
	@author Ceballos, Matias Lionel
	@date Abril 2019
*/
#include "cabezeras.h"

int main( int argc, char *argv[] ) {
	int socket = conectar();
	sesionActiva(socket);
	close(socket);
	return 0;
}

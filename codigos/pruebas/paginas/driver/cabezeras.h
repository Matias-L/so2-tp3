#ifndef CABEZERAS_H_
#define CABEZERAS_H_
/** @brief Archivo de cabezera que se debe incluir en todos los codigos fuentes

    
    @author Matias Lionel Ceballos
    @date Abril 2019
    */

#define TAM 			80
#define TAM2 			150
#define DIRECTORIO_SOCKET 	"../Servidor/socket"
#define DIRECTORIO_IMAGEN 	"./imagen/"
#define DIR_SOCK_TELE 		"./sockTele"
#define VERSION 		"1"
#define BYTES_STREAM 		1500
#define COLOR_ROJO 		"\x1b[0m"
#define COLOR_VERDE 		"\x1b[0m"
#define COLOR_AMARILLO 		"\x1b[0m"
#define COLOR_MAGENTA 		"\x1b[0m"
#define COLOR_AZUL		"\x1b[0m"
#define COLOR_RESET 		"\x1b[0m"
#define NRO_FRAGMENTOS 		51975
/** Librerias usados por los distintos codigos fuente
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <sys/sysinfo.h>
/** Funciones que escribí
*/


int conectar (void);
void sesionActiva(int);
int updateFirmware(int);
int startScanning(int);
int obtenerTelemetria(int);

#endif
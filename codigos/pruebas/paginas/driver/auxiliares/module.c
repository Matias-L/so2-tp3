#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Autor: Ceballos, Matias Lionel.
*/
int main(int argc, char const *argv[])
{
	char nombreArchivo[20];
	memset(nombreArchivo, '\0', 20);
	strcpy(nombreArchivo, argv[1]);
	char *token="";
	int accion=atoi(argv[2]);
	/*
	accion 0: instalar
	accion 1:desinstalar
	*/
	token = strtok(nombreArchivo, "."); //Elimino el nombre de archivo
	token=strtok(NULL, "\n"); 			//Busco extencion
		if(!strcmp(token,"ko")){
		printf("Extencion valida.\n");
			switch(accion){
				case 0:
					printf("Instalar\n");
				break;
				case 1:
					printf("DESinstalar\n");
				break;
				default:
					printf("Accion desconocida\n");
				break;
			}

		} //Fin extencion valida

	else{
		printf("Extencion no valida \n");
		printf("%s ::  %d", argv[1], accion);
	}

	return 0;
}

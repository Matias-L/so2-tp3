#!/usr/bin/perl

    use strict;
    use warnings;
    use CGI;
    use CGI::Carp qw(fatalsToBrowser); # Remove this in production

    my $q = new CGI;

    print $q->header();
print
'<form action="../index.html">
    <input type="submit" value="Volver al indice" />
</form>';
    # Output stylesheet, heading etc
    output_top($q);

    if ($q->param()) {
        # Parameters are defined, therefore the form has been submitted
        display_results($q);
    } else {
        # We're here for the first time, display the form
        output_form($q);
    }

    # Output footer and end html
    output_end($q);

    exit 0;

    #-------------------------------------------------------------

    # Outputs the start html tag, stylesheet and heading
    sub output_top {
        my ($q) = @_;
        print $q->start_html(
            -title => 'Consulta GOES',
            -bgcolor => 'white',
            -style => {
	        -code => '
                    /* Stylesheet code */
                    body {
                        font-family: verdana, sans-serif;
                    }
                    h2 {
                        color: darkblue;
                        border-bottom: 1pt solid;
                        width: 100%;
                    }
                    div {
                        text-align: right;
                        color: steelblue;
                        border-top: darkblue 1pt solid;
                        margin-top: 4pt;
                    }
                    th {
                        text-align: right;
                        padding: 2pt;
                        vertical-align: top;
                    }
                    td {
                        padding: 2pt;
                        vertical-align: top;
                    }
                    /* End Stylesheet code */
                ',
	    },
        );
        print $q->h2("Consulta GOES");
    }

    # Outputs a footer line and end html tags
    sub output_end {
        my ($q) = @_;
        print $q->div("SO2-TP3");
        print $q->end_html;
    }

    # Displays the results of the form
    sub display_results {
        my ($q) = @_;

        my $fecha_goes = $q->param('fecha');
        #my $hora_goes = $q->param('hora');

#        my $userage = $q->param('age');
#        my $usergender = $q->param('gender');
#        my @favourite_languages = sort $q->param('language');
#        my %sex = ('F' => 'girl', 'M' => 'boy');

#        print $q->h5("Resultados para la consulta $fecha_goes : $hora_goes hs");
        print $q->h5("Resultados para la consulta $fecha_goes");	
	
		#my @comando = "./listaGOES $fecha_goes$hora_goes/";
		#print $q->h5("CLI @comando");
		#my @result = qx(@comando);
		my $result = `aws s3 --recursive ls s3://noaa-goes16/ABI-L2-CMIPF/$fecha_goes --no-sign-request | grep 'C13'`;
        $result =~ s/$/<br>/mg; #reemplaza salto de linea por <br>
        #my @result = @comando;
        #@result =~ s/$/<br>/mg; #reemplaza salto de linea por <br>
		#$result =~ s/\s{2}/_/mg; #cada 2 espacios los reemplaza por un guion



		print $q->p("$result");
        #print $q->h4("fecha $fecha_goes");
        #print $q->h4("hora $hora_goes");
#        print $q->p("You are a $sex{$usergender}, and you are $userage years old.");
#        print $q->p("Your favourite languages are:");

#        print $q->table(
#            {-border => 1, -cellpadding => 3},
#            $q->Tr($q->td(\@favourite_languages)),
#        );
    }

    # Outputs a web form
    sub output_form {
        my ($q) = @_;
            my @values = ('00', '01', '02', '03', '04', '05', '06','07', '08', 
    	'09', '10', '11', '12', '13','14', '15', '16', '17','18','19','20','21','22','23');
        print $q->start_form(
            -name => 'main',
            -method => 'POST',
        );

        print $q->start_table;
        print $q->Tr(
          $q->td('Fecha (yyyy/ddd/)'),
          $q->td(
            $q->textfield(-name => "fecha", -size => 50)
          )
        );
#        print $q->Tr(
#          $q->td('Hora'),
#          $q->td(
#            $q->popup_menu(
#        -name    => 'hora',
#        -values  => \@values,
#        -default => '00'
#    )
#          )
#        );
#        print $q->Tr(
#          $q->td('Age:'),
#          $q->td(
#            $q->radio_group(
#              -name => 'age',
#              -values => [
#                  '0-12', '13-18', '18-30', '30-40', '40-50', '50-60', '60-70', '70+'
#              ],
#              -rows => 4,
#            )
#          )
#        );
#        my %genders = ('F' => 'Female', 'M' => 'Male');
#        print $q->Tr(
#          $q->td('Gender:'),
#          $q->td(
#            $q->popup_menu(
#              -name => 'gender',
#              -values => [keys %genders],
#              -labels => \%genders,
#            )
#          )
#        );
#        print $q->Tr(
#          $q->td('Favourite Languages:'),
#          $q->td(
#            $q->checkbox_group(
#              -name => 'language',
#              -values => ['Perl', 'C', 'C++', 'C#', 'Java', 'VB', 'Python', 'Delphi'],
#              -defaults => ['Perl'],
#              -columns => 2,
#            )
#          )
#        );
        print $q->Tr(
          $q->td($q->submit(-value => 'Submit')),
          $q->td('&nbsp;')
        );
        print $q->end_table;
        print $q->end_form;
    }
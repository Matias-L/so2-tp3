#!/usr/bin/perl
use CGI;
use strict;

my $PROGNAME = "GOES.pl";

my $cgi = new CGI();	
print "Content-type: text/html\n\n";

# If we're invoked directly, display the form and get out.
#
if (! $cgi->param("button") ) {
	DisplayForm();	#Invoca al sub DisplayForm
	exit;
}

#
# We're invoked from the form. Get the filename/handle.
#
my $fecha_goes = $cgi->param('fecha');
my $hora_goes = $cgi->param('hora');
#print "$fecha_goes"

#
# DisplayForm - spits out HTML to display our upload form.
#
sub DisplayForm {
print <<"HTML";
<html>
<head>
<title>Upload Form</title>
<body>
<h1>Upload Form</h1>
<form method="post" action="$PROGNAME" enctype="multipart/form-data">
  Fecha: <input type="date" name="fecha">
  Hora: <input type="number" name="hora">
  <input type="submit">
</form>
<h1> Modulos instalados </h1>
<p>$result</p>;
HTML
}
